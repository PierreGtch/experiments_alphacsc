import os
import numpy as np
import pickle
# import time
from tqdm import tqdm

import mne

from sobi import sobi ## https://github.com/davidrigie/sobi

from artefacts.utils_mne import raw_to_events



# set directories for saving results
datadir = os.path.expanduser('~/data/EEGData')
# savedir = os.path.basename(__file__).split('.')[0] + '_results'

## params : ##
file = '20190325_256Hz.raw.fif'
# file = '20190503_256Hz.raw.fif'
time_trials = 20 #seconds

filename = os.path.join(datadir, file)
out_filename = os.path.join(datadir, file+'.sobi_sources.pkl')

# load continuous data
print(f'loading data from {filename}...')
raw = mne.io.read_raw_fif(filename)
sfreq = raw.info['sfreq']

## split raw data into epochs ##
epochs = raw_to_events(raw, time_trials=time_trials)
epochs.drop_bad()

n = len(epochs)
_, n_channels, n_times, = epochs[0].get_data().shape
n_sources = n_channels
Ss = np.zeros((n,n_sources, n_times))
As = np.zeros((n,n_channels, n_sources))
Ws = np.zeros((n,n_sources, n_channels))
## main loop : ##
for e, S, A, W in tqdm(zip(epochs, Ss, As, Ws), desc='epochs'):
    S[:], A[:], W[:] = sobi(e)


## saving : ##
output = dict(S=Ss, A=As, W=Ws, filename=file, time_trials=time_trials)
with open(out_filename, 'wb') as f:
    pickle.dump(output, f)
print(f'output saevd at {out_filename}')
