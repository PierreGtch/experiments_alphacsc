"""
learn an atom describing the spikes using pre-epoched data
"""
import numpy as np
import time
import os
import matplotlib.pyplot as plt
import mne

from alphacsc.learn_d_z_multi_dilation import learn_d_z_multi_dilation
from alphacsc.learn_d_z_multi import learn_d_z_multi
from alphacsc.dilation import dilated_index
from alphacsc.utils import construct_X_multi, lil
from alphacsc.viz.plot_result import plot_result
from alphacsc.dilation import extend_uv


# set directories for saving results
datadir = os.path.expanduser('~/data/EEGData')
savedir = os.path.basename(__file__).split('.')[0] + '_results'

basename = os.path.join(datadir, '20190503_14h41_3-')

# get data :
raw_2ch = mne.io.read_raw_fif(basename+'1a-256Hz-raw.fif', preload=False)
epochs_1ch = mne.read_epochs(basename+'Ch1-1a-sorted-epo.fif', preload=False)
sfreq_2ch = raw_2ch.info['sfreq']
sfreq_1ch = epochs_1ch.info['sfreq']
events_2ch = (epochs_1ch.events.copy().astype('float') * sfreq_2ch / sfreq_1ch).astype('int32')
epochs_2ch = mne.Epochs(raw_2ch, events=events_2ch, tmin=epochs_1ch.tmin, tmax=epochs_1ch.tmax, baseline=None)
X = epochs_2ch.get_data()

# learn :
kwargs = dict(
    X=X, n_atoms=1, n_times_atom=X.shape[-1],
    n_iter=5, n_jobs=1,
    reg=1, D_init=np.zeros((0,131)),
    loss='l2',
    eps=1e-6,
    rank1=True, uv_constraint='separate', algorithm='batch',
    solver_z='omp', solver_z_kwargs=dict(n_nonzero_coefs=1, delta=0),
    solver_d='joint', solver_d_kwargs=dict(center_spikes=False),
    unbiased_z_hat=False, use_sparse_z=False, name="DL",
    window=True, raise_on_increase=False,
)
pobj, times, D_hat, z_hat, reg, = learn_d_z_multi(**kwargs)

# plot :
print(f'u = {D_hat[0,:2]}')
X_hat = construct_X_multi(z_hat, D_hat, n_channels=2)
print(X.shape, X_hat.shape, z_hat.shape, D_hat.shape)
plot_result(
    n_channels=2, n_dil=1, n_atoms=1, n_trials=X.shape[0], n_times=X.shape[-1],
    uv_hat=D_hat, X=X, X_hat=X_hat, z_hat=z_hat,
    max_times=X.shape[-1], max_trials=10, max_channels=2, max_atoms=1
)
