"""
remove artefacts then learn dict on clean data
"""
import numpy as np
from time import time
import os
import matplotlib.pyplot as plt
import mne

from alphacsc.learn_d_z_multi_dilation import learn_d_z_multi_dilation
from alphacsc.learn_d_z_multi import learn_d_z_multi
from alphacsc.dilation import dilated_index
from alphacsc.utils import construct_X_multi, lil
from alphacsc.viz.plot_result import plot_result
from alphacsc.dilation import extend_uv

from artefacts import cut_artefacts, list_to_padded_array
from artefacts.mne_utils import mask_to_annotation

# set directories for saving results
datadir = os.path.expanduser('~/data/EEGData')
savedir = os.path.basename(__file__).split('.')[0] + '_results'

basename = os.path.join(datadir, '20190503_14h41_3-')

# get data :
raw_2ch = mne.io.read_raw_fif(basename+'1a-256Hz-raw.fif', preload=False)
sfreq = raw_2ch.info['sfreq']

t0 = time()
print('removing artefacts...')
# remove artefacts :
kwargs_art = dict(
    t_window=5,
    max_ratio=2.,
    artefact_radius=5.,
    min_length=60.
)
Xs, intervals, mask = cut_artefacts(raw_2ch, **kwargs_art)
X, intervals = list_to_padded_array(Xs, intervals, n_times=int(kwargs_art['min_length']*sfreq))
X = X[:len(X)//10]
t1 = time()
print(f'...done in {t1-t0}s.')


# learn dict and encoding:
kwargs_dict = dict(
    X=X,
    n_atoms=10,
    n_times_atom=int(2*sfreq),
    n_iter=2, n_jobs=1,
    reg=1,
    loss='l2',
    eps=1e-6,
    rank1=True, uv_constraint='separate', algorithm='batch',
    solver_z='omp', solver_z_kwargs=dict(n_nonzero_coefs=30, delta=int(.5*sfreq)),
    solver_d='joint', solver_d_kwargs=dict(center_spikes=False),
    unbiased_z_hat=False, use_sparse_z=True, name="DL",
    window=True, raise_on_increase=False,
)

pobj, times, D_hat, z_hat, reg, = learn_d_z_multi(**kwargs_dict)

if kwargs_dict['use_sparse_z']:
    z_hat = lil.convert_from_list_of_lil(z_hat)

# plot :
print(f'u = {D_hat[0,:2]}')
X_hat = construct_X_multi(z_hat, D_hat, n_channels=2)
print(X.shape, X_hat.shape, z_hat.shape, D_hat.shape)
plot_result(
    n_channels=2, n_dil=1, n_atoms=kwargs_dict['n_atoms'], n_trials=X.shape[0], n_times=X.shape[-1],
    uv_hat=D_hat, X=X, X_hat=X_hat, z_hat=z_hat,
    max_times=X.shape[-1],
    max_trials=8,
    max_channels=2,
    max_atoms=kwargs_dict['n_atoms']
)
