import os
import matplotlib.pyplot as plt
import numpy as np
import time
# from scipy import signal, stats
from scipy.signal import resample
from scipy.io import loadmat
from scipy.fftpack import next_fast_len

import mne

from alphacsc.learn_d_z_multi_dilation import learn_d_z_multi_dilation
from alphacsc.learn_d_z_multi import learn_d_z_multi
from alphacsc.dilation import dilated_index
from alphacsc.utils import construct_X_multi, lil
from alphacsc.viz.plot_result import plot_result
from alphacsc.dilation import extend_uv



# processes contiguous dataset with multiclass spike learning (MC-Spike)


# set directories for saving results
datadir = os.path.expanduser('~/data/EEGData')
# savedir = os.path.basename(__file__).split('.')[0] + '_results'

##### load data ###########################################################

# load continuous data
file = '20190325_256Hz.raw.fif'
filename = os.path.join(datadir, file)
print(f'loading data from {filename}...')
raw = mne.io.read_raw_fif(filename)
sfreq = raw.info['sfreq']

print(raw.info) ## (?,?,)

################################################################################
## parameters ##
################################################################################
### shapes ###
# n_times = 200
times_atom = 1. #seconds
n_times_atom = int(times_atom * sfreq)
delta_t = .5 #seconds
delta = int(delta_t * sfreq)
n_atoms = 10
n_channels = 1
time_trials = 10. #seconds
# n_trials = 50
### data ###
# sparsity = 5e-3
# noise_scale =3e-3
### dilation ###
Q = 0
factor_max = 1.5 #8**.5
variability = .01
### training ###
n_iter = 5
n_jobs = 4
loss = 'l2'
# loss = 'dtw'
eps = 1e-6
reg = 1
# n_nonzero_coefs = 173
n_nonzero_coefs = 3
solver_d = 'joint'
# solver_d = 'alternate_adaptive'
solver_d_kwargs = dict(
    center_spikes=False,
    spikes_place=.2,#between 0 and 1
    )
solver_z = 'omp'
# solver_z = 'l-bfgs'
#solver_z = 'lgcd'
solver_z_kwargs = dict(n_nonzero_coefs=n_nonzero_coefs, delta=delta)
### display ###
max_trials = 3
max_channels = 3
max_atoms = 8
######

factor_max = factor_max if Q!=0 else 1.
# sparsity = sparsity/float(n_atoms)


################################################################################
## reshape X ##
################################################################################
events = np.arange(0, len(raw)/sfreq, time_trials) + time_trials/2.
annotations = mne.Annotations(events, np.zeros(len(events)), ['trial']*len(events))
raw.set_annotations(annotations)
events, event_id = mne.events_from_annotations(raw)
epochs = mne.Epochs(raw, events, event_id, -time_trials/2, time_trials/2)

t0 = time.time()
print('loading data...')
X = epochs.get_data().astype('float64')
print(f'...done in {time.time()-t0}s')
# (n_trials, n_channels, n_times)
n_trials, n_channels, n_times = X.shape

## only keep 10% :
n_trials = n_trials//40
X = X[:n_trials]

print(f"X.shape={X.shape}")


################################################################################
## solving ##
################################################################################
X_hat = None
# print(X.dtype, D0.dtype)
kwargs = dict(
    #     Q=Q, factor_max=factor_max, variability=variability,
    X=X, n_atoms=n_atoms, n_times_atom=n_times_atom,
    n_iter=n_iter, n_jobs=n_jobs,
    lmbd_max='fixed', reg=reg, loss=loss,
    loss_params=dict(gamma=.1, sakoe_chiba_band=10, ordar=10),
    rank1=True, uv_constraint='separate', eps=eps,
    algorithm='batch', algorithm_params=dict(),
    detrending=None, detrending_params=dict(),
    solver_z=solver_z, solver_z_kwargs=solver_z_kwargs,
    solver_d=solver_d, solver_d_kwargs=solver_d_kwargs,
    D_init=None, D_init_params=dict(),
    unbiased_z_hat=False, use_sparse_z=False,
    stopping_pobj=None, raise_on_increase=False,
    verbose=10, callback=None, random_state=None, name="DL",
    window=True, sort_atoms=False
)
pobj, times, D_hat, z_hat, reg, = learn_d_z_multi(
# pobj, times, D_hat, z_hat, reg, X_hat = learn_d_z_multi_dilation(
**kwargs
)



################################################################################
## saving ##
################################################################################
import pickle
out_file = 'new_result.pkl'
rez = dict(D_hat=D_hat, z_hat=z_hat, kwargs=kwargs)
pickle.dump(rez, open(out_file, "wb"))



################################################################################
## plottinig ##
################################################################################
_,_, lengths, _,_ = dilated_index(n_times_atom, Q=Q, factor_max=factor_max, variability=variability)
n_dil = len(lengths)
uv_ext = extend_uv(D_hat, lengths, n_channels=n_channels)
X_hat = construct_X_multi(z=z_hat, D=uv_ext, n_channels=n_channels)
z_hat = z_hat if not lil.is_list_of_lil(z_hat) else lil.convert_from_list_of_lil(z_hat)

# print(f"pobj {pobj}")
# print(f"times {times}")
# print(f"z_hat[0] : {z_hat[0]}")
# for i in range(n_atoms*n_dil):
#     print(f"z_hat[0][{i}] : {z_hat[0,i:200]}")
print(f"z_hat's sparsity : {(z_hat!=0).mean()}")
print(f"z_hat's shape : {z_hat.shape}")

plot_result(n_channels, n_atoms, n_trials, n_times, n_dil=n_dil, uv_hat=uv_ext, X=X, X_hat=X_hat, z_hat=z_hat,
            max_times=4000, max_trials=max_trials, max_channels=max_channels, max_atoms=max_atoms)
