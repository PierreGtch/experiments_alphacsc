# import os
# import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
# import time
# from scipy.signal import resample
# from scipy.io import loadmat

from alphacsc.learn_d_z_multi_dilation import learn_d_z_multi_dilation
from alphacsc.learn_d_z_multi import learn_d_z_multi
from alphacsc.dilation import dilated_index
from alphacsc.utils import construct_X_multi, lil
from alphacsc.viz.plot_result import plot_result
from alphacsc.dilation import extend_uv


################################################################################
## parameters ##
################################################################################
DO_PLOT = True
### shapes ###
# n_times = 4000000
n_times = 5000
n_times_atom = 100
delta = 00
n_atoms = 3
n_channels = 15
n_trials = 10
z = np.zeros((n_trials, n_atoms, n_times-n_times_atom+1))
### data ###
sparsity = .0001 #5e-3
noise_scale =5e-2
### dilation ###
Q = 0
factor_max = 2.
variability = .4
### training ###
n_iter = 10
n_jobs = 1
loss = 'l2'
# loss = 'dtw'
eps = 1e-2
reg = .1
n_nonzero_coefs = 173
# n_nonzero_coefs = int(sparsity*np.prod(z.shape))
solver_d = 'joint'
# solver_d = 'alternate_adaptive'
solver_z = 'omp'
# solver_z = 'pass'
### display ###
max_trials = 2
max_channels = 3
max_atoms = 5
######

solver_z_kwargs = dict(n_nonzero_coefs=n_nonzero_coefs, delta=delta)
factor_max = factor_max if Q!=0 else 1.
sparsity = sparsity/float(n_atoms)

################################################################################
## data generation ##
################################################################################
normalize = lambda x: x/np.linalg.norm(x)
def sin_gauss(loc, sig, f):
    return lambda t : normalize(stats.norm(loc=0, scale=sig).pdf(t) * np.sin(2*np.pi*(t-loc)*f))
triangle = lambda t: -normalize(t/2+.5)
square = lambda t: normalize(-np.concatenate([[0.,],np.ones(t.size-2),[0.,]]))
spike = sin_gauss(loc=.63,sig=.2,f=1)
gaussian = lambda t: normalize(stats.norm(loc=0, scale=.3).pdf(t))
waves = sin_gauss(loc=0,sig=.4,f=1.5)
waves2 = sin_gauss(loc=0,sig=.4,f=3)
waves3 = sin_gauss(loc=0,sig=.4,f=7)

## creating dictionary :
vf = [gaussian, triangle, square][:n_atoms]
# vf = [waves, waves2, waves3, spike, gaussian][:n_atoms]
t = np.linspace(-1, 1, n_times_atom)
v = np.stack([f(t) for f in vf], axis=0)
u = np.random.rand(n_atoms, n_channels) + .2
u /= np.sum(u**2, axis=1 ,keepdims=True)**.5
uv = np.concatenate([u,v], axis=1)

## creating codes and dilations :
assert Q==0, 'not implemented Q!=0'
idx = np.random.randint(np.prod(z.shape), size=n_nonzero_coefs)
w = n_times_atom*(np.random.rand(n_nonzero_coefs)+.5)
z[np.unravel_index(idx, z.shape)] = w
# dils_choice = np.exp((np.random.rand(*z.shape)-.5)*2*np.log(factor_max))

## creating signals :
assert Q==0, 'not implemented Q!=0'
X = construct_X_multi(z, D=uv, n_channels=n_channels)
# X = omp.construct_X_multi(z.reshape(1,-1), uv, n_channels).reshape(n_channels, -1)

# X = np.zeros((n_trials, n_channels, n_times))
# for i_tr in range(n_trials):
#     for i_a in range(n_atoms):
#         for i_ti in range(z.shape[-1]):
#             i = (i_tr,i_a,i_ti)
#             # print(i, sp_mask.shape)
#             if sp_mask[i]:
#                 xi = np.outer(u[i_a], vf[i_a](np.linspace(-1,1,int(n_times_atom*dils_choice[i]))))
#                 # truncate if too long :
#                 xi = xi[:,:min(xi.shape[1], X.shape[-1]-i_ti)]
#                 X[i_tr,:,i_ti:i_ti+xi.shape[1]] += z[i] * xi

## adding gaussian noise to signal :
X += noise_scale * np.random.randn(*X.shape)



################################################################################
## solving ##
################################################################################
pobj, times, D_hat, z_hat, reg = learn_d_z_multi(
# pobj, times, D_hat, z_hat, reg, X_hat = learn_d_z_multi_dilation(
    # Q=Q, factor_max=factor_max, variability=variability,
    n_atoms=n_atoms, n_times_atom=n_times_atom,
    X=X, n_iter=n_iter, n_jobs=n_jobs,
    lmbd_max='fixed', reg=reg, loss=loss,
    loss_params=dict(gamma=.1, sakoe_chiba_band=10, ordar=10),
    rank1=True, uv_constraint='separate', eps=eps,
    algorithm='batch', algorithm_params=dict(),
    detrending=None, detrending_params=dict(),
    solver_z=solver_z, solver_z_kwargs=solver_z_kwargs,
    solver_d=solver_d, solver_d_kwargs=dict(),
    D_init=None, D_init_params=dict(),
    unbiased_z_hat=False, use_sparse_z=True,
    stopping_pobj=None, raise_on_increase=False,
    verbose=10, callback=None, random_state=None, name="DL",
    window=True, sort_atoms=False
)

################################################################################
## plottinig ##
################################################################################
if DO_PLOT:
    _,_, lengths, _,_ = dilated_index(n_times_atom, Q=Q, factor_max=factor_max, variability=variability)
    n_dil = len(lengths)
    uv_ext = extend_uv(D_hat, lengths, n_channels=n_channels)
    X_hat = construct_X_multi(z=z_hat, D=uv_ext, n_channels=n_channels)
    z_hat = z_hat if not lil.is_list_of_lil(z_hat) else lil.convert_from_list_of_lil(z_hat)

    print(f"z_hat's sparsity : {(z_hat!=0).mean()}")
    print(f"z_hat's shape : {z_hat.shape}")

    assert Q==0, 'not implemented Q!=0'
    plot_result(
        n_channels, n_atoms, n_trials, n_times, n_dil=n_dil,
        uv_hat=uv_ext, X=X, X_hat=X_hat, z_hat=z_hat,z=z, uv=uv,
        max_times=10000
        )
