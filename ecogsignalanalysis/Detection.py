#! /usr/bin/python3
# coding: utf-8

# import sys
# import csv
import mne
import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from alphacsc.learn_d_z_multi import learn_d_z_multi
from alphacsc.utils import construct_X_multi

parser = argparse.ArgumentParser(prog='Detection',description='EAWL')
parser.add_argument('file',type=str,metavar='epoch_file',help="Path to the video file")
parser.add_argument('atome',type=int,metavar='NAtome',help="Number of atome")

args = parser.parse_args()

epoch_file=args.file



epochs = mne.read_epochs(epoch_file)
evoked = epochs.average()
epochs_data = epochs.get_data()
# idx = np.argmax(evoked.data.max(axis=1))
# X = epochs_data[:, idx, :]
X = epochs_data
X /= X.max()
t = np.linspace(0, 0.5,np.size(epochs_data, 2))
print(X.shape, epochs_data.shape, evoked)

################################################################################
## parameters ##
################################################################################
### shapes ###
# n_times = 200
n_times_atom = X.shape[-1]
n_atoms = 3
n_channels = X.shape[1]
n_trials = X.shape[0]
### training ###
n_iter = 30
n_jobs = 4
loss = 'l2'
# loss = 'dtw'
eps = 1e-6
reg = .1
# solver_d = 'joint'
solver_d = 'alternate_adaptive'
# algorithm = 'batch'
algorithm = 'greedy'
######
print(X.shape, n_times_atom)
pobj, times, dictionary, code, reg,  = learn_d_z_multi(
    X=X, n_atoms=n_atoms, n_times_atom=n_times_atom,
    n_iter=n_iter, n_jobs=n_jobs,
    lmbd_max='fixed', reg=reg, loss=loss,
    loss_params=None,#dict(gamma=.1, sakoe_chiba_band=10, ordar=10),
    rank1=True, uv_constraint='separate', eps=eps,
    algorithm=algorithm, algorithm_params=dict(),
    detrending=None, detrending_params=dict(),
    solver_z='l-bfgs', solver_z_kwargs=dict(),
    solver_d=solver_d, solver_d_kwargs=dict(),
    D_init=None, D_init_params=dict(),
    unbiased_z_hat=False, use_sparse_z=False,
    stopping_pobj=None, raise_on_increase=False,
    verbose=10, callback=None, random_state=None, name="DL",
    window=True, sort_atoms=False
)
print(X.shape, code.shape, dictionary.shape)
X_hat = construct_X_multi(code, dictionary, n_channels=1)
dictionary = dictionary[:,1:]
residual = X - X_hat
print('residual = {}'.format(residual))
print(f'code = shape {code.shape}\n {code}')

# #pl.plot(1e3 * epochs.times, dictionary[0].T)
# #pl.plot(epochs.times, dictionary[1].T)
# pl.show()

fig = plt.figure(constrained_layout=True)
gs = gridspec.GridSpec(3, 3, figure=fig)
ax = fig.add_subplot(gs[0, :])
ax.plot(1e3 * evoked.times, evoked.data.T)
# ax.plot(1e3 * evoked.times, X[0,0], label='X')
# ax.plot(1e3 * evoked.times, X_hat[0,0], label='X_hat')
ax.plot(1e3 * evoked.times, X[:,0].mean(axis=0), label='X mean')
ax.plot(1e3 * evoked.times, X_hat[:,0].mean(axis=0), label='X_hat mean')
print(X_hat.shape)
ax.legend()
ax.set_xlabel('time [s]')
plt.title('evoked')

ax1 = fig.add_subplot(gs[1, :])
ax1.plot(epochs.times, dictionary.T)
ax1.set_xlabel('time [s]')
plt.title('spikes')

ax2 = fig.add_subplot(gs[2, 0])
ax2.plot(epochs.times, dictionary[0].T)
ax2.set_xlabel('time [s]')
plt.title('spike 0')
ax3 = fig.add_subplot(gs[2, 1])
ax3.plot(epochs.times, dictionary[1].T)
ax3.set_xlabel('time [s]')
plt.title('spike 1')
ax4 = fig.add_subplot(gs[2, 2])
ax4.plot(epochs.times, dictionary[2].T)
ax4.set_xlabel('time [s]')
plt.title('spike 2')

plt.show()
