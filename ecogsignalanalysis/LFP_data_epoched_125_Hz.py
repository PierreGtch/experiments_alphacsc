#! /usr/bin/python3
# coding: utf-8

# import sys
# import csv
# import mne
# import argparse
import os
from scipy.io import loadmat
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec


from alphacsc.learn_d_z_multi import learn_d_z_multi
from alphacsc.learn_d_z import learn_d_z
from alphacsc.utils import construct_X_multi
from alphacsc.utils import construct_X


epoch_file = '~/stage_inria_2020/src/ecogsignalanalysis-/AWL_matlab/Experiments/data/LFP_data_epoched_125_Hz.mat'

m = loadmat(os.path.expanduser(epoch_file))
t = m['t'].ravel()
X = m['X'].T
print(X.shape, t.shape) # (169, 1000) (1000,)

################################################################################
## parameters ##
################################################################################
### shapes ###
times = 8
n_times = X.shape[-1]
times_atom = 3
# n_times = 200
n_times_atom = int(n_times*times_atom/times)
n_atoms = 5
n_trials = X.shape[0]
### training ###
n_iter = 50
n_jobs = 7
loss = 'l2'
# loss = 'dtw'
eps = 1e-6
reg = 10
# solver_d = 'joint'
# solver_d = 'alternate_adaptive'
# algorithm = 'batch'
algorithm = 'greedy'

ta = t[:n_times_atom]

######
# X = X.reshape(n_trials, 1, n_times_atom)
# pobj, times, dictionary, code, reg,  = learn_d_z_multi(
pobj, times, dictionary, code, reg,  = learn_d_z(
    X=X, n_atoms=n_atoms, n_times_atom=n_times_atom,
    n_iter=n_iter, n_jobs=n_jobs,
    lmbd_max='fixed', reg=reg,
    # loss=loss, loss_params=None,#dict(gamma=.1, sakoe_chiba_band=10, ordar=10),
    # rank1=True, uv_constraint='separate', eps=eps,
    # algorithm=algorithm, algorithm_params=dict(),
    solver_z='l-bfgs', solver_z_kwargs=dict(),
    # solver_d=solver_d,
    # D_init=None, D_init_params=dict(),
    # unbiased_z_hat=False, use_sparse_z=False,
    # stopping_pobj=None, raise_on_increase=False,
    # verbose=10, callback=None, random_state=None, name="DL",
    # window=True, sort_atoms=False
)
if code.shape[0]!=n_atoms:
    X = X.reshape(n_trials, n_times_atom)
    code = code.reshape(n_trials, n_atoms).T
    dictionary = dictionary[:,int(dictionary.shape[-1]!=n_times_atom):]

print(X.shape, code.shape, dictionary.shape)
# X_hat = construct_X_multi(code, dictionary, n_channels=1)
X_hat = construct_X(code.reshape(n_atoms, n_trials, -1), dictionary)
# X_hat = np.dot(code, dictionary)
residual = X - X_hat
print('residual = {}'.format(residual))
print(f'code = shape {code.shape}\n {code}')

# #pl.plot(1e3 * epochs.times, dictionary[0].T)
# #pl.plot(epochs.times, dictionary[1].T)
# pl.show()

fig = plt.figure(constrained_layout=True)
gs = gridspec.GridSpec(3, n_atoms, figure=fig)
ax = fig.add_subplot(gs[0, :])
ax.plot(t, X[0], label='X')
ax.plot(t, X_hat[0], label='X_hat')
ax.plot(t, X[:].mean(axis=0), label='X mean')
ax.plot(t, X_hat[:].mean(axis=0), label='X_hat mean')
ax.legend()
ax.set_xlabel('time [s]')
plt.title('evoked')

# ax1 = fig.add_subplot(gs[1, :])
# ax1.plot(t, dictionary.T)
# ax1.set_xlabel('time [s]')
# plt.title('spikes')

for i in range(n_atoms):
    ax2 = fig.add_subplot(gs[1, i])
    ax2.plot(ta, dictionary[i].T)
    ax2.set_xlabel('time [s]')
    plt.title(f'spike {i}')

ax4 = fig.add_subplot(gs[2, :])
code2 = code.reshape(n_atoms, n_trials, (n_times-n_times_atom+1)).transpose(1,0,2).reshape(n_trials, n_atoms*(n_times-n_times_atom+1))
im = ax4.matshow(code2, aspect="auto")
plt.colorbar(im)
ax4.set_xlabel('atoms')
ax4.set_ylabel('trials')
plt.title('code')

plt.show()
