import numpy as np
# from scipy import optimize, signal
import matplotlib.pyplot as plt

from alphacsc.update_d_multi import center_spikes_uv
from alphacsc.utils import construct_X_multi


n_times_atom = 20
n_times = 100
n_channels = 1

t = np.linspace(0, 1, n_times_atom)
v0 = np.abs(t-.2) - .8
v1 = np.abs(2*t-1.6) - 1.6
uv = np.ones((2, 1+n_times_atom))
uv[0,1:] = v0
uv[1,1:] = v1

z = np.zeros((n_channels, 2, n_times-n_times_atom+1))
z[0,0,30] = 1
z[0,1,50] = 1

X = construct_X_multi(z, D=uv, n_channels=n_channels)

fig0 = plt.figure(0)
fig1 = plt.figure(1)

plt.plot(uv[:,1:].T, label='old')
plt.figure(0)
plt.plot(X[0,0], label='true X')

new_uv = center_spikes_uv(uv, z, n_channels)
new_X = construct_X_multi(z, D=new_uv, n_channels=n_channels)

plt.plot(new_X[0,0], label='centered X')
plt.legend()
plt.figure(1)
plt.plot(new_uv[:,1:].T, label='centered')
plt.legend()

plt.show()
