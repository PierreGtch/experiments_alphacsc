import numpy as np
from scipy.signal import resample, decimate


def _get_best_fft_lengths(old_length, new_lengths_ranges):
    """
    Tries to find optimal new lengths to resample faster (computed with fft)

    Args:
        old_length: int
        new_lengths_ranges: array, shape (ndil,2,)
            ranges of lengths among which the new lengths must be seearched

    Returns:
        new_lenths: array, shape (ndil,)

    """
    ## TODO : faire mieux !!!!
    return np.rint(np.mean(new_lengths_ranges, axis=1)).astype(int)


def _dilation_factors(Q, beta):
    """
    Computes the logarithmically spaced dilation factors
        [beta**q for q in range(-Q, Q+1)]

    Args:
        Q: int
            ~ number of coefficients
        beta: float
            resolution between dilations

    Returns:
        coefs: array, shape (2*Q+1,)
            dilation coeffitients
    """
    assert isinstance(beta, (float,int)), 'parameter "beta" must be a float'
    assert beta<=1, "can't have parameter beta>1"
    print('beta : ',beta)
    return beta**np.arange(-Q, Q+1, 1)

def _dilation_factors_max(Q, factor_max):
    """
    Computes the logarithmically spaced dilation factors
    between 1/factor_max and factor_max
        [beta**q for q in range(-Q, Q+1)]

    Args:
        Q: int
            ~ number of coefficients
        factor_max: float
            maximum dilation factor

    Returns:
        coefs: array, shape (2*Q+1,)
            dilation coeffitients
    """
    beta = factor_max**(-1/Q) if Q!=0 else 1
    return _dilation_factors(Q, beta), beta

def dilated_index(n_times, dilation_factors=None, Q=None, beta=None, factor_max=None, variability=None, verbose=False):
    """
    Signal truncated if n_times/gamma_i is not integer

    Args:
        n_times: int
            number of time indices
        dilation_factors: array, shape (n_dil,)
            see _dilation_factors()
        Q: int
            ~ number of coefficients
        beta: float
            resolution between dilations
        factor_max: float
            maximum dilation factor
        variability: None or float
            variability factor, see _get_best_fft_lengths()

    Returns:
        oldToNew_idx: array, shape (n_dil,n_times,)
            description
        newToOld_idx: array, shape (n_dil,max(lengths),)
            description
        lengths: array, shape (n_dil,)
            description
        mask: array, shape (n_dil,max(lengths),)
            description
        dilation_factors: array, shape (n_dil,)
            description
    """
    eps = 1e-10
    if dilation_factors is None:
        assert not (beta is None and factor_max is None), 'beta and factor_max can not be both None'
        if beta is None:
            exact_dilation_factors, beta = _dilation_factors_max(Q, factor_max)
        else:
            exact_dilation_factors = _dilation_factors(Q, beta)
        exact_dilation_factors = exact_dilation_factors.reshape(-1,1)

        variability = 0. if variability is None else variability
        lengths_max = np.rint(n_times * exact_dilation_factors * beta**(-variability) + eps).astype(int)
        lengths_min = np.rint(n_times * exact_dilation_factors * beta**(+variability) + eps).astype(int)
        lengths_ranges  = np.concatenate([lengths_min, lengths_max], axis=1)
        lengths_temp = _get_best_fft_lengths(n_times, lengths_ranges).ravel()
        # remove duplicates:
        lengths,last = [], None
        for l in lengths_temp:
            if l!=last:
                last = l
                lengths.append(l)
        lengths = np.array(lengths)

        dilation_factors = (lengths/float(n_times)).reshape(-1,1)
        if verbose:
            print('beta', beta**variability, beta, 1/beta**variability)
            print('lengths_range\n', lengths_ranges)
            print('dilation_factors (old,new)\n', np.concatenate([exact_dilation_factors, dilation_factors], axis=1))
            import matplotlib.pyplot as plt
            for l in lengths_ranges.T:
                plt.plot(l)
            plt.show(block=True)
    else:
        lengths = np.rint(n_times * dilation_factors + eps).astype(int).ravel()

    n_dil = len(dilation_factors)
    mask = np.zeros((n_dil, max(lengths)))
    for i,l in enumerate(lengths):
        mask[i,:l] += 1

    oldToNew_idx = np.zeros((n_dil, n_times))
    for i,l in enumerate(lengths):
        _,oldToNew_idx[i,:] = resample(np.zeros(l), n_times, np.arange(l))

    newToOld_idx = np.zeros(mask.shape) - 1
    for i,l in enumerate(lengths):
        _,newToOld_idx[i,:l] = resample(np.zeros(n_times), l, np.arange(n_times))

    if verbose:
        print('lengths\n', lengths)
        print('max lengths', max(lengths))
        print("oldToNew_idx\n", oldToNew_idx)
        print("newToOld_idx\n", newToOld_idx)
        print("mask\n", mask)
    return oldToNew_idx, newToOld_idx, lengths, mask, dilation_factors



###############################################################################

def extend_X(X, lengths):
    """
    A short description.

    A bit longer description.

    Args:
        X : array, shape (n_trials, n_channels, n_times)
            description
        lengths: array, shape (n_dil,)
            description

    Returns:
        X_ext : array, shape (n_trials * n_dil, n_channels, n_times * max_dil)
            description

    """
    n_trials, n_channels, n_times = X.shape
    n_dil, = lengths.shape
    X_ext = np.zeros((n_trials, n_dil, n_channels, max(lengths)), dtype=X.dtype,)
    for i,l in enumerate(lengths):
        X_ext[:,i,:,:l] = resample(X[:,:,:], l, axis=2)
    return X_ext.reshape(n_trials*n_dil,n_channels,max(lengths))

def extend_uv(uv, lengths, n_channels):
    """
    A short description.

    Args:
        uv : array, shape (n_atoms, n_channels + n_times_atom)
            description
        lengths: array, shape (n_dil,)
            description
        n_channels: int
            description

    Returns:
        uv_ext : array, shape (n_dil * n_atoms , n_channels + n_times_atom * max_dil)
            description

    """
    n_atoms, n_times_atom = uv.shape
    n_times_atom -= n_channels
    n_dil, = lengths.shape
    uv_ext = np.zeros((n_dil,n_atoms,n_channels+max(lengths)), dtype=uv.dtype,)
    uv_ext[:,:,:n_channels] = uv[:,:n_channels]
    for i,l in enumerate(lengths):
        uv_ext[i,:,n_channels:n_channels+l] = resample(uv[:,n_channels:], l, axis=1)
    return uv_ext.reshape(n_dil*n_atoms, n_channels+max(lengths))

def unextend_grad(grad, lengths, n_channels, n_times_atom):
    """
    Experimental

    Args:
        grad : array, shape (n_dil * n_atoms, n_channels, n_times_atom * max_dil)
            gradient relative to the extended dictionary
        lengths: array, shape (n_dil,)
            lengths of the dilated atoms
        n_times_atom: int
            length of the unextended atoms
        n_channels: int
            number of channels

    Returns:
        grad_unext : array, shape (n_atoms , n_channels, n_times_atom)
            gradient relative to the un-extended dictionary

    """
    n_dil_n_atoms, n_channels, n_times_atom_ext = grad.shape
    n_dil, = lengths.shape
    assert n_dil_n_atoms%n_dil == 0
    n_atoms = n_dil_n_atoms//n_dil
    grad = grad.reshape(n_dil, n_atoms, n_channels, n_times_atom_ext)
    grad_unext = np.zeros((n_dil,n_atoms,n_channels,n_times_atom), dtype=grad.dtype,)
    for grad_i, grad_unext_i, li in zip(grad, grad_unext, lengths):
        grad_unext_i[:,:,:] = resample(grad_i[:,:,:li], n_times_atom, axis=2)
    return grad_unext.sum(axis=0)

def reshape_z(z_ext1, dilated_idx, lengths, n_times_atom):
    """
    The coeffictients z have two representations:
    - z_ext1 used in update_z(...)
    - z_ext2 used in update_uv(...)

    reshape_z(...) converts z_ext1 to z_ext2

    Args:
        z_ext1 : array, shape (n_trials, n_dil * n_atoms, n_times - n_times_atom * min_dil + 1)
            description
        dilated_idx: array, shape (n_dil, n_times,)
            description
        lengths: array, shape (n_dil,)
            description
        n_times_atom: int
            description

    Returns:
        z_ext2 : array, shape (n_trials * n_dil, n_atoms, n_times * max_dil - n_times_atom + 1)
            description

    """
    n_dil, = lengths.shape
    n_dil, n_times, = dilated_idx.shape
    n_trials, n_atoms, n_times_minus_atom = z_ext1.shape
    assert n_atoms%n_dil==0
    n_atoms //= n_dil

    # int_idx = np.rint(dilated_idx).astype(int).reshape(1,n_dil,1,n_times)
    int_idx = np.rint(dilated_idx).astype(int)

    z_ext1_reshaped = z_ext1.reshape(n_trials, n_dil, n_atoms, n_times_minus_atom)
    z_ext2 = np.zeros((n_trials,n_dil,n_atoms,max(lengths)-n_times_atom+1), dtype=z_ext1.dtype,)

    # TODO: more efficient
    for i_dil in range(n_dil):
        z_ext1_i = z_ext1_reshaped[:,i_dil]
        z_ext2_i = z_ext2[:,i_dil]
        int_idx_i = int_idx[i_dil]
        for i_times in range(n_times_minus_atom):
            # print(z_ext2_i.shape, int_idx_i[i_times].shape, z_ext1_i[:,:,i_times].shape)
            z_ext2_i[:,:,int_idx_i[i_times]] += z_ext1_i[:,:,i_times]
    # for i_times in range(n_times_minus_atom):
    #     # z_ext2[:,:,:,int_idx[:,i_times]] += z_ext1[:,:,:,i_times]
    #     # print(int_idx[:,:,:,i_times:i_times+1].shape, z_ext2.shape)
    #     val = np.take_along_axis(z_ext2, int_idx[:,:,:,i_times:i_times+1], axis=3) + z_ext1_reshaped[:,:,:,i_times:i_times+1]
    #     np.put_along_axis(z_ext2, int_idx[:,:,:,i_times:i_times+1], val, axis=3)
    return z_ext2.reshape(n_trials*n_dil,n_atoms, max(lengths)-n_times_atom+1)



###############################################################################
## main ##
###############################################################################
if __name__=='__main__':
    from time import time
    import matplotlib.pyplot as plt
    from attr_dict import AttrDict
    from scipy import signal, stats
    from alphacsc.utils import construct_X_multi

    p = AttrDict({
        'Q' : 50,
        'factor_max' : 1.5,
        'variability' : .4,
        'n_times' : 200,
        'n_times_atom' : 20,
        'n_trials' : 1,
        'n_channels' : 1,
        'n_atoms' : 1,

        'sparsity': 1e-2,
    })
    for k,v in p.items():
        print(k,v)

    _, _, lengths_uv, mask_uv, df = dilated_index(
        p.n_times_atom, Q=p.Q, factor_max=p.factor_max, variability=p.variability)
    oldToNew_X, _, lengths_X, _, _ = dilated_index(p.n_times, dilation_factors=1/df)



    #########################
    ## create uv, z_ext1
    #########################
    ## uv ##
    gaussian = stats.norm(loc=0, scale=.3).pdf
    t = np.linspace(-1, 1, p.n_times_atom)
    v = np.stack([gaussian(t) for _ in range(p.n_atoms)], axis=0)
    u = np.random.rand(p.n_atoms, p.n_channels) + .2
    u /= np.sum(u**2, axis=1 ,keepdims=True)**.5
    uv = np.concatenate([u,v], axis=1)
    ## z_ext1 ##
    z_ext1 = np.random.rand(p.n_trials, len(lengths_uv)*p.n_atoms, p.n_times-max(lengths_uv)+1)
    z_ext1 = z_ext1*10+10
    # z_ext1[:,:,:] = np.arange(z_ext1.shape[-1]).reshape(1,1,-1)
    # sp_mask = np.ones(z.shape)
    sp_mask = np.random.rand(*z_ext1.shape) < p.sparsity
    z_ext1[~sp_mask] = 0


    #########################
    ## compute X_hat, uv_ext, X_ext, z_ext2
    #########################
    print("extend_uv()...")
    t0 = time()
    uv_ext = extend_uv(uv, lengths_uv, n_channels=p.n_channels)
    print(f"...done in {time()-t0}s")
    print(uv.shape, uv_ext.shape)

    X_hat = construct_X_multi(z_ext1, D=uv_ext, n_channels=p.n_channels)

    print("extend_X()...")
    t0 = time()
    X_ext = extend_X(X_hat, lengths_X)
    print(f"...done in {time()-t0}s")
    print(X_hat.shape, X_ext.shape)

    print("reshape_z()...")
    t0 = time()
    z_ext2 = reshape_z(z_ext1, oldToNew_X, lengths_X, p.n_times_atom)
    print(f"...done in {time()-t0}s")
    print(z_ext1.shape, z_ext2.shape)


    #########################
    ## check extend_X
    #########################
    fig = plt.figure()
    ax = fig.add_subplot(2,1,1)
    for i,l in enumerate(lengths_X):
        ax.plot(
        X_ext.reshape(p.n_trials,len(lengths_X),p.n_channels,max(lengths_X))[0,i,0],
        label=f'X_ext{i} (length={l})',
        marker="" if l!=p.n_times else "o")
    ax = fig.add_subplot(2,1,2)
    ax.plot(X_hat[0,0],label=f'X_hat')
    plt.legend()

    idx = list(lengths_X).index(p.n_times)
    print(idx, len(lengths_X))
    X_ext_1 = X_ext[len(lengths_X)*np.arange(p.n_trials)+idx,:,:p.n_times]
    diff=X_hat-X_ext_1
    fig = plt.figure()
    fig.suptitle("X_hat vs X_ext[scale=1]")
    ax = fig.add_subplot(3,1,1)
    ax.set_title("X_hat")
    ax.matshow(X_hat.reshape(p.n_trials*p.n_channels,p.n_times))
    ax = fig.add_subplot(3,1,2)
    ax.set_title("X_ext_1")
    ax.matshow(X_ext_1.reshape(p.n_trials*p.n_channels,p.n_times))
    ax = fig.add_subplot(3,1,3)
    ax.set_title("diff")
    im = ax.matshow(np.abs(diff.reshape(p.n_trials*p.n_channels,p.n_times)))
    plt.colorbar(im)

    assert np.allclose(X_hat, X_ext_1)



    # #########################
    # ## check reshape_z
    # #########################
    # X_ext2 = construct_X_multi(z_ext2, D=uv, n_channels=p.n_channels)
    # X_hat2_exploded = construct_X_multi(z_ext2, D=uv, n_channels=p.n_channels)
    # X_hat2 = np.zeros(X_hat.shape)
    # # for
    #
    # diff=X_ext-X_ext2
    # fig = plt.figure()
    # fig.suptitle("X_ext vs X_ext2(computed with z_ext2)")
    # ax = fig.add_subplot(3,1,1)
    # ax.set_title('X_ext')
    # ax.matshow(X_ext.reshape(p.n_trials*len(lengths_X)*p.n_channels,max(lengths_X)))
    # ax = fig.add_subplot(3,1,2)
    # ax.set_title('X_hat2')
    # ax.matshow(X_hat2.reshape(p.n_trials*len(lengths_X)*p.n_channels,max(lengths_X)))
    # ax = fig.add_subplot(3,1,3)
    # im = ax.matshow(np.abs(diff).reshape(p.n_trials*len(lengths_X)*p.n_channels,max(lengths_X)))
    # plt.colorbar(im)
    # plt.show()
    # assert np.allclose(X_ext2, X_ext)


    fig = plt.figure()
    fig.suptitle("z_ext1, z_ext2")
    ax = fig.add_subplot(2,1,1)
    im = ax.matshow(z_ext1.reshape(p.n_trials,len(lengths_X),p.n_atoms,-1)[0,:,0,:])
    plt.colorbar(im)
    ax = fig.add_subplot(2,1,2)
    im = ax.matshow(z_ext2.reshape(p.n_trials,len(lengths_X),p.n_atoms,-1)[0,:,0,:])
    plt.colorbar(im)

    fig = plt.figure()
    for i,l in enumerate(lengths_uv):
        plt.plot(
            uv_ext.reshape(len(lengths_uv),p.n_atoms , p.n_channels+max(lengths_uv))[i,0,p.n_channels:],
            label=f'uv{i} (length={l})',
            marker="" if l!=p.n_times_atom else "o")
    plt.legend()


    plt.show()

    #### attention !!! : X_ext != const_X(z_ext2, uv)

    ### construct_X_multi(z_ext1, D=uv_ext) = X_ext ==? X_ext2 = construct_X_multi(z_ext2, D=uv)
    ### construct_X_multi(z_ext1, D=extend_uv(uv)) = X_ext ==? X_ext2 = construct_X_multi(reshape_z(z_ext1), D=uv)

    # extend_X(construct_X_multi(z, D=uv)) ==? construct_X_multi(z_duplicate, D=uv)
