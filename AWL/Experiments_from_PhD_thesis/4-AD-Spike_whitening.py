import os
import numpy as np
import time
# from scipy import signal, stats
from scipy.signal import resample
from scipy.io import loadmat

from alphacsc.learn_d_z_mcem import learn_d_z_weighted
# from alphacsc.dilation import dilated_index
from alphacsc.utils import construct_X
from alphacsc.viz.plot_result import plot_result
# from alphacsc.dilation import extend_uv



# processes contiguous dataset with multiclass spike learning (MC-Spike)


# set directories for saving results
datadir = os.path.expanduser('~/stage_inria_2020/src/AWL/Experiments/data')
savedir = os.path.basename(__file__).split('.')[0] + '_results'

##### load data ###########################################################
freq = 1250

# load continuous data
file = 'LFP_data_contiguous_'+str(freq)+'_Hz.mat'
filename = os.path.join(datadir, file)
t0 = time.time()
print(f'loading data from {filename}...')
data = loadmat(filename)
X = data['X'].ravel().astype('float64')  ## (3901178, 1)
# X_th=data['X'][:500000]< -150; np.sum(X_th[:-1] & ~X_th[1:]) # 173
# X_th=data['X']< -150; np.sum(X_th[:-1] & ~X_th[1:])  ## 726
# X = X[:500000]
sfreq = int(data['sfreq'])
# t = np.linspace(0,1/sfreq*(X.shape[0]-1), X.shape[0])

# load markers of isolated spikes
file = 'markers_isolated_'+str(freq)+'_Hz.mat'
filename = os.path.join(datadir, file)
markers = loadmat(filename)['markers'].ravel()
print(f'...done in {time.time()-t0}s')


new_sfreq = 125
t0 = time.time()
print(f"resampling from {sfreq}Hz to {new_sfreq}Hz...")
X = resample(X, int(len(X)*new_sfreq/sfreq))
markers = markers*new_sfreq//sfreq
sfreq = new_sfreq
print(f'...done in {time.time()-t0}s')

print(X.shape, markers.shape, sfreq) ## (156047,) (169,) 50

################################################################################
## parameters ##
################################################################################
### shapes ###
# n_times = 200
times_atom = 1.5 #seconds
n_times_atom = int(times_atom * sfreq)
n_atoms = 8
n_trials = 1
### training ###
n_iter_global = 10
n_iter_optim = 10
n_iter_mcmc = 10
n_jobs = 8
reg = 1
solver_z = 'l-bfgs'
# solver_z = 'lgcd'
solver_z_kwargs = dict()
### display ###
max_trials = 3
max_channels = 3
max_atoms = 8
######


################################################################################
## initialize atom with most energetic spike ##
################################################################################
D0 = None
# t_left = -0.05     # [seconds] time until negative peak
# # t_dist = 0.2       # [seconds] duration of negative spike wave
# # t_right = times_atom+t_left # [seconds] time after negative peak
# sel = np.arange(int(t_left*sfreq), int(t_left*sfreq)+n_times_atom).astype(int)
# print(sel.shape, X.shape, markers.shape)
# val = 0
# ind = None
# for i in markers:
#     current_val = np.linalg.norm(X[sel+i])
#     if current_val > val:
#         ind = i
#         val = current_val
#
# D0 = np.zeros((1,1+n_times_atom), dtype=X.dtype)
# D0[:,0] = 1
# D0[:,1:] = X[sel+ind]

################################################################################
## reshape X ##
################################################################################
# (n_trials, n_channels, n_times)
n = X.size
n_times = n//n_trials if n%n_trials==0 else n//(n_trials-1)
print(f'reshaping signal from ({n},) to ({(n_trials, n_times)})')
new_X = np.zeros((n_trials, n_times), dtype=X.dtype)
for i in range(n_trials):
    new_X[i,:min(n_times, n-i*n_times)] = X[i*n_times:(i+1)*n_times]
X, new_X = new_X, None

print(f"X.shape={X.shape}")


################################################################################
## solving ##
################################################################################
X_hat = None
# print(X.dtype, D0.dtype)
D_hat, z_hat, tau, = learn_d_z_weighted(
    X=X, n_atoms=n_atoms, n_times_atom=n_times_atom,
    n_iter_global=n_iter_global, n_iter_optim=n_iter_optim, n_iter_mcmc=n_iter_mcmc, n_jobs=n_jobs,
    lmbd_max='fixed', reg=reg,
    solver_z=solver_z, solver_z_kwargs=solver_z_kwargs,
    solver_d_kwargs=dict(),
    verbose=10, callback=None, random_state=None,
)

################################################################################
## saving ##
################################################################################
# import pickle
# out_file = 'result.pkl'
# rez = dict(D_hat=D_hat, Z_hat=z_hat, sfreq=sfreq, Xshape=X.shape )
# pickle.dump(rez, open(out_file, "wb"))


################################################################################
## plottinig ##
################################################################################
uv_ext = np.concatenate([np.ones((n_atoms, 1)), D_hat], axis=1)
X_hat = construct_X(z=z_hat, ds=uv_ext).reshape(n_trials, 1, -1)
z_hat = z_hat.transpose(1, 0, 2)
X = X.reshape(n_trials, 1, -1)

# print(f"pobj {pobj}")
# print(f"times {times}")
# print(f"z_hat[0] : {z_hat[0]}")
# for i in range(n_atoms*n_dil):
#     print(f"z_hat[0][{i}] : {z_hat[0,i:200]}")
print(f"z_hat's sparsity : {(z_hat!=0).mean()}")
print(f"z_hat's shape : {z_hat.shape}")

n_channels = 1
n_dil = 1
plot_result(n_channels, n_atoms, n_trials, n_times, n_dil=n_dil, uv_hat=uv_ext, X=X, X_hat=X_hat, z_hat=z_hat,
            max_times=4000, max_trials=max_trials, max_channels=max_channels, max_atoms=max_atoms)
