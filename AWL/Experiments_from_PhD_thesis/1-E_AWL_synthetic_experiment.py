import matplotlib.pyplot as plt
import numpy as np
from scipy import signal, stats
from itertools import permutations

from alphacsc.learn_d_z_multi_dilation import learn_d_z_multi_dilation
# from alphacsc.utils import construct_X_multi
from alphacsc.dilation import dilated_index


################################################################################
## parameters ##
################################################################################
### shapes ###
n_times = 200
n_times_atom = 64
n_atoms = 3
n_channels = 3
n_trials = 5
### data ###
sparsity = 5e-3
noise_scale =3e-3
### dilation ###
Q = 20
factor_max = 2.
variability = .4
### training ###
n_iter = 5
n_jobs = 8
loss = 'l2'
# loss = 'dtw'
eps = 1e-2
reg = 3
# solver_d = 'joint'
solver_d = 'alternate_adaptive'
### display ###
max_trials = 2
max_channels = 3
max_atoms = 5
######

factor_max = factor_max if Q!=0 else 1.
sparsity = sparsity/float(n_atoms)

################################################################################
## data generation ##
################################################################################
def normalize(x):
    return x/np.linalg.norm(x)
def sin_gauss(loc, sig, f):
    def fun(t):
        return normalize(stats.norm(loc=0, scale=sig).pdf(t) * np.sin(2*np.pi*(t-loc)*f))
    return fun

triangle = lambda t: normalize(t/2+.5)
spike = sin_gauss(loc=.63,sig=.2,f=1)
gaussian = lambda t: normalize(stats.norm(loc=0, scale=.3).pdf(t))
waves = sin_gauss(loc=0,sig=.4,f=1.5)
waves2 = sin_gauss(loc=0,sig=.4,f=3)

## creating dictionary :
vf = [gaussian, waves, waves2, spike][:n_atoms]
# vf = [sin_gauss(sig=.1, f=1, loc=.5) for _ in range(n_atoms)]
# random :
# vf = [sin_gauss(sig=np.random.rand(), f=1/(.2+np.random.rand()), loc=np.random.rand()) for _ in range(n_atoms)]

t = np.linspace(-1, 1, n_times_atom)
v = np.stack([f(t) for f in vf], axis=0)
u = np.random.rand(n_atoms, n_channels) + .2
u /= np.sum(u**2, axis=1 ,keepdims=True)**.5
uv = np.concatenate([u,v], axis=1)

## creating codes and dilations :
z = np.random.rand(n_trials, n_atoms, n_times-n_times_atom+1)
z = z*2 + 3 ## z in [3,5]
sp_mask = np.random.rand(*z.shape) < sparsity
z[~sp_mask] = 0
dils_choice = np.exp((np.random.rand(*z.shape)-.5)*2*np.log(factor_max))
# atoms_choice = np.random.randint(0,n_atoms,z.shape)

## creating signals :
X = np.zeros((n_trials, n_channels, n_times))
for i_tr in range(n_trials):
    for i_a in range(n_atoms):
        for i_ti in range(z.shape[-1]):
            i = (i_tr,i_a,i_ti)
            # print(i, sp_mask.shape)
            if sp_mask[i]:
                xi = np.outer(u[i_a], vf[i_a](np.linspace(-1,1,int(n_times_atom*dils_choice[i]))))
                # truncate if too long :
                xi = xi[:,:min(xi.shape[1], X.shape[-1]-i_ti)]
                X[i_tr,:,i_ti:i_ti+xi.shape[1]] += z[i] * xi

## adding gaussian noise to signal :
X += noise_scale * np.random.randn(*X.shape)



################################################################################
## solving ##
################################################################################
pobj, times, D_hat, z_hat, reg, X_hat = learn_d_z_multi_dilation(
    X=X, n_atoms=n_atoms, n_times_atom=n_times_atom, Q=Q,
    factor_max=factor_max, variability=variability,
    n_iter=n_iter, n_jobs=n_jobs,
    lmbd_max='fixed', reg=reg, loss=loss,
    loss_params=dict(gamma=.1, sakoe_chiba_band=10, ordar=10),
    rank1=True, uv_constraint='separate', eps=eps,
    algorithm='batch', algorithm_params=dict(),
    detrending=None, detrending_params=dict(),
    solver_z='l-bfgs', solver_z_kwargs=dict(),
    solver_d=solver_d, solver_d_kwargs=dict(),
    D_init=None, D_init_params=dict(),
    unbiased_z_hat=False, use_sparse_z=False,
    stopping_pobj=None, raise_on_increase=False,
    verbose=10, callback=None, random_state=None, name="DL",
    window=True, sort_atoms=False
)

################################################################################
## plottinig ##
################################################################################
_,_, lengths, _,_ = dilated_index(n_times, Q=Q, factor_max=factor_max, variability=variability)
n_dil = len(lengths)
# print(f"pobj {pobj}")
# print(f"times {times}")
print(f"z[0] : {z[0]}")
print(f"z_hat[0] : {z_hat[0]}")
for i in range(n_atoms*n_dil):
    print(f"z_hat[0][{i}] : {z_hat[0,i:200]}")
print(f"z_hat's sparsity : {(z_hat!=0).mean()}")
print(f"z_hat's shape : {z_hat.shape}")


v_hat = -D_hat[:,n_channels:]
## align atoms :
dist_mat = np.zeros((n_atoms, n_atoms))
for i in range(n_atoms):
    for j in range(n_atoms):
        dist_mat[i,j] = np.sum((v[i]-v_hat[j])**2, axis=0)
best_dist , best_align = np.inf, None
for align in permutations(np.arange(n_atoms)):
    dist = np.trace(dist_mat[:,align])
    if dist<best_dist:
        best_dist = dist
        best_align = align
v_hat = v_hat[best_align,:]


n_t = min(n_trials, max_trials)
n_c = min(n_channels, max_channels)
n_a = min(n_atoms, max_atoms)

fig = plt.figure()
fig.suptitle("atoms (uv and uv_hat)")
for i in range(n_a):
    ax = plt.subplot(n_a,2,i*2+1)
    ax.plot(v[i], label=f"true atom {i}")
    plt.legend()
for i in range(n_a):
    ax = plt.subplot(n_a,2,i*2+2)
    ax.plot(v_hat[i], label=f"predicted atom {i}")
    plt.legend()


fig = plt.figure()
fig.suptitle("signals (X and X_hat)")
i_c = 0
for i_t in range(n_t):
    ax = plt.subplot(n_t,1,i_t + 1)
    ax.set_title(f"signal{i_t}, channel{i_c}")
    ax.plot(X[i_t,i_c], label=f"true signal{i_t}")
    ax.plot(X_hat[i_t,i_c], label=f"computed signal{i_t}")
    plt.legend()


# z_hat = z_hat.reshape(n_trials, n_dil, n_atoms, -1)
# fig = plt.figure()
# fig.suptitle("coefficients (z_hat)")
# for i_t in range(n_t):
#     ax = plt.subplot(n_t,n_a,i_t*n_a +i_a + 1)
#     ax.set_title(f"trial{i_t} | coeffs of atom{i_a}")
#     im = ax.matshow(z_hat[i_t,:,i_a,:])
#     plt.colorbar(im)

plt.show()
