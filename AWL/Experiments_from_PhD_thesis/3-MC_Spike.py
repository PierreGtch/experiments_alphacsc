import os
import matplotlib.pyplot as plt
import numpy as np
# from scipy import signal, stats
from scipy.io import loadmat
from itertools import permutations

from alphacsc.learn_d_z_multi_dilation import learn_d_z_multi_dilation
from alphacsc.dilation import dilated_index


# processes contiguous dataset with multiclass spike learning (MC-Spike)


# set directories for saving results
datadir = os.path.expanduser('~/stage_inria_2020/src/AWL/Experiments/data')
savedir = os.path.basename(__file__).split('.')[0] + '_results'

##### load data ###########################################################
freq = 1250

# load continuous data
file = 'LFP_data_contiguous_'+str(freq)+'_Hz.mat'
filename = os.path.join(datadir, file)
data = loadmat(filename)
X = data['X']
sfreq = data['sfreq']
t = np.linspace(0,1/sfreq*(X.shape[0]-1), X.shape[0])

# load markers of isolated spikes
file = 'markers_isolated_'+str(freq)+'_Hz.mat'
filename = os.path.join(datadir, file)
markers = loadmat(filename)['markers']

print(X.shape, t.shape) # (3901178, 1) (3901178, 1, 1)

################################################################################
## parameters ##
################################################################################
### shapes ###
# n_times = 200
times_atom = 2 #seconds
n_times_atom = int(times_atom * sfreq)
n_atoms = 8
# n_channels = 1
n_trials = 1
### data ###
sparsity = 5e-3
noise_scale =3e-3
### dilation ###
Q = 20
factor_max = 2.
variability = .4
### training ###
n_iter = 5
n_jobs = 8
loss = 'l2'
# loss = 'dtw'
eps = 1e-2
reg = 3
# solver_d = 'joint'
solver_d = 'alternate_adaptive'
### display ###
max_trials = 2
max_channels = 3
max_atoms = 5
######

factor_max = factor_max if Q!=0 else 1.
sparsity = sparsity/float(n_atoms)

# #### select isolated spike with max energy for initialization ############

# spike parameters
t_left = -0.05     # [seconds] time until negative peak
t_dist = 0.2       # [seconds] duration of negative spike wave
t_right = 2+t_left # [seconds] time after negative peak

# # select spike with maximal energy
# ind = (1:length(t))'
# sel = ind(t>(t(markers(1))+t_left) & t < (t(markers(1))+t_right))
# t_spike = t(sel) - t(markers(1))
# d = zeros(size(t_spike))
# sel = sel - markers(1)
# ind = 0
# val = 0
# for i=1:length(markers)
#     current_val = norm(X(sel+markers(i)))
#     if (current_val > val)
#         ind = i
#         val = current_val
#     end
# end
# d = X(sel+markers(ind))
#
# # plot template spike (used to initialize algorithm)
# PLOT = 1
# if PLOT
#     figure
#     plot(t_spike,d)
#     title('spike used for initialization')
#     xlabel('time [s]')
# end


##### learn spike representation ##########################################

# set parameters
par=struct
par.D=d
par.ndist = floor(t_dist*sfreq)   # samples of peak
par.ncenter = sum(t_spike<=0)     # samples until max peak
par.iter=10                       # iterations per representation
par.maxK=8                        # maximal spikes of representation
par.alpha=0.1                     # detection threshold (WARNING: calculation becomes slow if alpha < 0.1 !!!)
par.verbose=true

res=mexMCSpike(X,par)


##### save ################################################################
# if ~exist(savedir,'dir')
#     mkdir(savedir)
# end
# save([savedir 'res_MCSpike'],'res','t_spike')
