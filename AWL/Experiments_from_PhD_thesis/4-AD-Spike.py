import os
import matplotlib.pyplot as plt
import numpy as np
import time
# from scipy import signal, stats
from scipy.signal import resample
from scipy.io import loadmat
from scipy.fftpack import next_fast_len

from alphacsc.learn_d_z_multi import learn_d_z_multi
from alphacsc.learn_d_z_multi_dilation import learn_d_z_multi_dilation
from alphacsc.dilation import dilated_index
from alphacsc.utils import construct_X_multi, lil
from alphacsc.viz.plot_result import plot_result
from alphacsc.dilation import extend_uv



# processes contiguous dataset with multiclass spike learning (MC-Spike)


# set directories for saving results
datadir = os.path.expanduser('~/stage_inria_2020/src/AWL/Experiments/data')
savedir = os.path.basename(__file__).split('.')[0] + '_results'

##### load data ###########################################################
freq = 1250

# load continuous data
file = 'LFP_data_contiguous_'+str(freq)+'_Hz.mat'
filename = os.path.join(datadir, file)
t0 = time.time()
print(f'loading data from {filename}...')
data = loadmat(filename)
X = data['X'].ravel().astype('float64')  ## (3901178, 1)
# X_th=data['X'][:500000]< -150; np.sum(X_th[:-1] & ~X_th[1:]) # 173
# X_th=data['X']< -150; np.sum(X_th[:-1] & ~X_th[1:])  ## 726
# X = X[:500000]
sfreq = int(data['sfreq'])
# t = np.linspace(0,1/sfreq*(X.shape[0]-1), X.shape[0])

# load markers of isolated spikes
file = 'markers_isolated_'+str(freq)+'_Hz.mat'
filename = os.path.join(datadir, file)
markers = loadmat(filename)['markers'].ravel()
print(f'...done in {time.time()-t0}s')


new_sfreq = 125
t0 = time.time()
print(f"resampling from {sfreq}Hz to {new_sfreq}Hz...")
X = resample(X, int(len(X)*new_sfreq/sfreq))
markers = markers*new_sfreq//sfreq
sfreq = new_sfreq
print(f'...done in {time.time()-t0}s')

print(X.shape, markers.shape, sfreq) ## (156047,) (169,) 50

################################################################################
## parameters ##
################################################################################
### shapes ###
# n_times = 200
times_atom = 2. #seconds
n_times_atom = int(times_atom * sfreq)
delta_t = .7 #seconds
delta = int(delta_t * sfreq)
n_atoms = 8
n_channels = 1
n_trials = 50
### data ###
# sparsity = 5e-3
# noise_scale =3e-3
### dilation ###
Q = 0
factor_max = 1.5 #8**.5
variability = .01
### training ###
n_iter = 30
n_jobs = 8
loss = 'l2'
# loss = 'dtw'
eps = 1e-6
reg = 1
# n_nonzero_coefs = 173
n_nonzero_coefs = 726*2//n_trials
solver_d = 'joint'
# solver_d = 'alternate_adaptive'
solver_d_kwargs = dict(
    center_spikes=True,
    spikes_place=.2,#between 0 and 1
    )
solver_z = 'omp'
#solver_z = 'l-bfgs'
#solver_z = 'lgcd'
solver_z_kwargs = dict(n_nonzero_coefs=n_nonzero_coefs, delta=delta)
### display ###
max_trials = 3
max_channels = 3
max_atoms = 8
######

factor_max = factor_max if Q!=0 else 1.
# sparsity = sparsity/float(n_atoms)


################################################################################
## initialize atom with most energetic spike ##
################################################################################
D0 = None
# t_left = -0.05     # [seconds] time until negative peak
# # t_dist = 0.2       # [seconds] duration of negative spike wave
# # t_right = times_atom+t_left # [seconds] time after negative peak
# sel = np.arange(int(t_left*sfreq), int(t_left*sfreq)+n_times_atom).astype(int)
# print(sel.shape, X.shape, markers.shape)
# val = 0
# ind = None
# for i in markers:
#     current_val = np.linalg.norm(X[sel+i])
#     if current_val > val:
#         ind = i
#         val = current_val
#
# D0 = np.zeros((1,1+n_times_atom), dtype=X.dtype)
# D0[:,0] = 1
# D0[:,1:] = X[sel+ind]

################################################################################
## reshape X ##
################################################################################
# (n_trials, n_channels, n_times)
n = X.size
n_times = n//n_trials if n%n_trials==0 else n//(n_trials-1)
n_times = next_fast_len(n_times) ## faster fft
n_trials = n//n_times if n%n_times==0 else n//n_times + 1
print(f'reshaping signal from ({n},) to ({(n_trials, n_channels, n_times)})')
new_X = np.zeros((n_trials, n_channels, n_times), dtype=X.dtype)
for i in range(n_trials):
    new_X[i,:,:min(n_times, n-i*n_times)] = X[i*n_times:(i+1)*n_times]
X, new_X = new_X, None

## only keep 10% :
n_trials = n_trials//1
X = X[:n_trials]

print(f"X.shape={X.shape}")


################################################################################
## solving ##
################################################################################
X_hat = None
# print(X.dtype, D0.dtype)
kwargs = dict(
    #     Q=Q, factor_max=factor_max, variability=variability,
    X=X, n_atoms=n_atoms, n_times_atom=n_times_atom,
    n_iter=n_iter, n_jobs=n_jobs,
    lmbd_max='fixed', reg=reg, loss=loss,
    loss_params=dict(gamma=.1, sakoe_chiba_band=10, ordar=10),
    rank1=True, uv_constraint='separate', eps=eps,
    algorithm='batch', algorithm_params=dict(),
    detrending=None, detrending_params=dict(),
    solver_z=solver_z, solver_z_kwargs=solver_z_kwargs,
    solver_d=solver_d, solver_d_kwargs=solver_d_kwargs,
    D_init=D0, D_init_params=dict(),
    unbiased_z_hat=False, use_sparse_z=True,
    stopping_pobj=None, raise_on_increase=False,
    verbose=10, callback=None, random_state=None, name="DL",
    window=True, sort_atoms=False
)
pobj, times, D_hat, z_hat, reg, = learn_d_z_multi(
# pobj, times, D_hat, z_hat, reg, X_hat = learn_d_z_multi_dilation(
**kwargs
)



################################################################################
## saving ##
################################################################################
import pickle
out_file = 'new_result.pkl'
rez = dict(D_hat=D_hat, z_hat=z_hat, kwargs=kwargs)
pickle.dump(rez, open(out_file, "wb"))



################################################################################
## plottinig ##
################################################################################
_,_, lengths, _,_ = dilated_index(n_times_atom, Q=Q, factor_max=factor_max, variability=variability)
n_dil = len(lengths)
uv_ext = extend_uv(D_hat, lengths, n_channels=n_channels)
X_hat = construct_X_multi(z=z_hat, D=uv_ext, n_channels=n_channels)
z_hat = z_hat if not lil.is_list_of_lil(z_hat) else lil.convert_from_list_of_lil(z_hat)

# print(f"pobj {pobj}")
# print(f"times {times}")
# print(f"z_hat[0] : {z_hat[0]}")
# for i in range(n_atoms*n_dil):
#     print(f"z_hat[0][{i}] : {z_hat[0,i:200]}")
print(f"z_hat's sparsity : {(z_hat!=0).mean()}")
print(f"z_hat's shape : {z_hat.shape}")

plot_result(n_channels, n_atoms, n_trials, n_times, n_dil=n_dil, uv_hat=uv_ext, X=X, X_hat=X_hat, z_hat=z_hat,
            max_times=4000, max_trials=max_trials, max_channels=max_channels, max_atoms=max_atoms)
